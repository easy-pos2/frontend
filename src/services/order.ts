import http from './http'
import type { Receipt } from '@/type/Receipt'
import type { ReceiptItem } from '@/type/ReceiptItem'

type receiptDto = {
    orderItems: {
        productId: number
        qty: number
    }[]
    userId: number
}

function addOrder(receipt: Receipt,receiptItems: ReceiptItem) {
  const receiptDto: receiptDto = {
    orderItems: [],
    userId: 0
  }
  receiptDto.userId = receipt.userId
  receiptDto.orderItems = receiptItems.map((item) => {
    return {
        productId: item.productId,
        qty:item.unit
    }
  })
    return http.post('/order', receiptDto)
}
export default { adddOrder }
